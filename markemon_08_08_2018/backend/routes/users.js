const express = require('express');
const router = express.Router();
const passport = require('passport');
var jwt = require('jsonwebtoken');
const config = require('./../config/database');
const checkAuth = require('../middleware/check-auth');


const User = require('../models/users');

// Register
router.post('/register',(req,res,next)=>{
    let newUser = new User({
    name : req.body.name,
    username : req.body.username,
    password : req.body.password,
    email : req.body.email
   });
   User.addUser(newUser,(err,user)=>{
    if(err)
    {
        res.json({
            success : false,
            msg:"Failed to register user"
        });
    }
    else{
        res.json({
            success : true,
            msg : "User register"
        });
    }

   })

    // res.send('register..');
});

//Authenticate
router.post('/authenticate',(req,res,next)=>{
   let email = req.body.email;
   let password = req.body.password;
    console.log(email);
    console.log(password);
   User.getUserByUsername(email, (err,user)=>{
    if(err)
    {
        res.json({
            success : false,
            msg : err
        });
    }
    if(!user)
    {
        res.json({
            success : false,
            msg : "User not found"
        });
    }
    else{
        User.comparePassword(password, user.password,(err, isMatch)=>{
        
            if(err)
            {
                res.json({
                    success : false,
                    msg : err
                });
            }
            if(isMatch)
            {
            
                var token = jwt.sign({ id: user._id },config.secret,{
                    expiresIn : 604800 // 1 weeks
                });

                res.json({
                    success : true,
                    token : token,
                    user : {
                        id : user._id,
                        name : user.name,
                        username : user.username,
                        email : user.email
                    }
                });
            }
            else{
                res.json({
                    success : false,
                    msg : "Wrong details"
                });
            }
        });
    }
   });
  

});

router.get('/profile',checkAuth,
  function(req, res) {
     const userId = req.userData.id;
     User.findById(userId, function (err, user) {
         console.log(user);
         if(err)
         {
            res.json({
                success : false,
                msg : err
            });  
         }
         if(!user)
         {
            res.json({
                success : false,
                msg : "User not found"
            });
         }
         else{
            res.json({
                success : true,
                msg : "You are authorized"
            });
         }
        });
  });


  router.get('/me', function(req, res, next) {
    var token = req.headers['token'];

    if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });
    
    jwt.verify(token, config.secret, function(err, decoded) {
      if (err) return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
      
      User.findById(decoded.id, 
      { password: 0 }, // projection
      function (err, user) {
        if (err) return res.status(500).send("There was a problem finding the user.");
        if (!user) return res.status(404).send("No user found.");
        // res.status(200).send(user); Comment this out!
        next(user); // add this line
      });
    });
  });

//Validate
router.get('/validate',(req,res,next)=>{
    res.send('validate');
});

//Login
router.post('/login',(req,res,next)=>{
    res.send('login..');
});

module.exports = router;