var mysql   = require('mysql');
// Create connection
const db = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : 'markemon'
});

module.exports = db;