const express = require('express');
const path =  require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport =  require('passport');
const mongoose = require('mongoose');
//const mysql  = require('mysql');

//const config = require('./config/mysql_database');

const mysql_config = require('./config/mysql_database');

//Mongoose database connection
/*mongoose.connect(config.database);
mongoose.connection.on('connected',()=>{
    console.log('Mongodb database connection');
});
mongoose.connection.on('error',(err)=>{
    console.log('Mongodb database not connection and erro is : '+err);
});*/

// MySql database connection
mysql_config.connect((err)=>{
    if(err)
    {
        console.log(err);
    }
    else{
        console.log('Mysql database connected');
    }
});

const app = express();

const users = require('./routes/users');

const port = 3000;
app.use(cors());


// Set static path
app.use(express.static(path.join(__dirname,'public')));


// Body Parser middleware 
app.use(bodyParser.json());

// Passport middleware
app.use(passport.initialize());
app.use(passport.session());

require('./config/passport')(passport);


// All user module
app.use('/users',users);

//Index route
app.get('/',(req,res)=>{
    res.send('Invalid Endpoint');
});


// Start server
app.listen(port,()=>{
    console.log('server started on 3000');
});
