var mysql   = require('mysql');
// Create connection
const db = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : 'census_db_v1'
});

module.exports = db;