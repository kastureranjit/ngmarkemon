-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 01, 2018 at 04:59 PM
-- Server version: 5.7.23-0ubuntu0.16.04.1
-- PHP Version: 5.6.33-3+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mediadb_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(10) UNSIGNED NOT NULL,
  `CODE` varchar(3) DEFAULT NULL,
  `LSTMODDT` date DEFAULT NULL,
  `LSTMODTM` varchar(8) DEFAULT NULL,
  `NAME` varchar(20) DEFAULT NULL,
  `ROWOPER` varchar(1) DEFAULT NULL,
  `USERID` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `CODE`, `LSTMODDT`, `LSTMODTM`, `NAME`, `ROWOPER`, `USERID`) VALUES
(1, 'IND', '0000-00-00', '        ', 'INDIA               ', ' ', '  '),
(2, 'USA', '0000-00-00', '        ', 'UNITED STATE AMERICA', ' ', '  '),
(3, 'NEP', '0000-00-00', '        ', 'NEPAL               ', ' ', '  '),
(4, 'BHU', '0000-00-00', '        ', 'BHUTAN              ', ' ', '  '),
(5, 'UAE', '0000-00-00', '        ', 'UNITED ARAB EMIRATES', ' ', '  '),
(6, 'UK ', '2015-08-04', '16:01:08', 'UNITED KINGDOM      ', 'E', '01'),
(7, 'PAK', '0000-00-00', '        ', 'PAKISTAN            ', ' ', '  '),
(8, 'SOM', '0000-00-00', '        ', 'SULTANATE OF OMAN   ', ' ', '  '),
(9, 'CHN', '0000-00-00', '        ', 'CHINA               ', ' ', '  '),
(10, 'CAN', '0000-00-00', '        ', 'CANADA              ', ' ', '  '),
(11, 'BRA', '0000-00-00', '        ', 'BRAZIL              ', ' ', '  '),
(12, 'BGD', '0000-00-00', '        ', 'BANGLADESH          ', ' ', '  '),
(13, 'FRA', '0000-00-00', '        ', 'FRANCE              ', ' ', '  '),
(14, 'HKG', '0000-00-00', '        ', 'HONGKONG            ', ' ', '  '),
(15, 'INA', '0000-00-00', '        ', 'INDONESIA           ', ' ', '  '),
(16, 'IRN', '0000-00-00', '        ', 'IRAN                ', ' ', '  '),
(17, 'IRQ', '0000-00-00', '        ', 'IRAQ                ', ' ', '  '),
(18, 'ITA', '0000-00-00', '        ', 'ITALY               ', ' ', '  '),
(19, 'ISR', '0000-00-00', '        ', 'ISRAEL              ', ' ', '  '),
(20, 'JPN', '0000-00-00', '        ', 'JAPAN               ', ' ', '  '),
(21, 'KUW', '0000-00-00', '        ', 'KUWAIT              ', ' ', '  '),
(22, 'MEX', '0000-00-00', '        ', 'MEXICO              ', ' ', '  '),
(23, 'SGP', '0000-00-00', '        ', 'SINGAPORE           ', ' ', '  '),
(24, 'SPA', '0000-00-00', '        ', 'SPAIN               ', ' ', '  '),
(25, 'SRL', '0000-00-00', '        ', 'SRI LANKA           ', ' ', '  '),
(26, 'POL', '0000-00-00', '        ', 'POLAND              ', ' ', '  '),
(27, 'KSA', '0000-00-00', '        ', 'KINGDOM OF S.ARABIA ', ' ', '  '),
(28, 'TAI', '0000-00-00', '        ', 'TAIWAN              ', ' ', '  '),
(29, 'BAH', '0000-00-00', '        ', 'BAHRAIN             ', ' ', '  '),
(30, 'MUS', '0000-00-00', '        ', 'MUSCAT              ', ' ', '  '),
(31, 'VET', '0000-00-00', '        ', 'VIETNAM             ', ' ', '  '),
(32, 'EGP', '0000-00-00', '        ', 'EGYPT               ', ' ', '  '),
(33, 'ZMB', '0000-00-00', '        ', 'ZIMBABWE            ', ' ', '  '),
(34, 'BUR', '0000-00-00', '        ', 'BURMA               ', ' ', '  '),
(35, 'SA ', '0000-00-00', '        ', 'SOUTH AFRICA        ', ' ', '  '),
(36, 'MAU', '0000-00-00', '        ', 'MAURITIUS           ', ' ', '  '),
(37, 'MAL', '0000-00-00', '        ', 'MALAYSIA            ', ' ', '  '),
(38, 'INT', '0000-00-00', '        ', 'INTERNATIONAL       ', ' ', '  '),
(39, 'THA', '0000-00-00', '        ', 'THAILAND            ', ' ', '  '),
(40, 'RUS', '0000-00-00', '        ', 'RUSSIA              ', ' ', '  '),
(41, 'KEN', '0000-00-00', '        ', 'KENYA               ', ' ', '  '),
(42, 'GER', '0000-00-00', '        ', 'GERMANY             ', ' ', '  '),
(43, 'EUR', '0000-00-00', '        ', 'EUROPE              ', ' ', '  '),
(44, 'NET', '2010-12-23', '18:44:09', 'NETHERLAND          ', 'E', '01'),
(45, 'BEL', '0000-00-00', '        ', 'BELGIUM             ', ' ', '  '),
(46, 'AUS', '0000-00-00', '        ', 'AUSTRALIA           ', ' ', '  '),
(47, 'SWE', '0000-00-00', '        ', 'SWEDEN              ', ' ', '  '),
(48, 'MOZ', '0000-00-00', '        ', 'MOZAMBIQUE          ', ' ', '  '),
(49, 'SEY', '0000-00-00', '        ', 'SEYCHELLES          ', ' ', '  '),
(50, 'BRU', '0000-00-00', '        ', 'BRUNEI              ', ' ', '  '),
(51, 'KOR', '0000-00-00', '        ', 'KOREA               ', ' ', '  '),
(52, 'MLD', '0000-00-00', '        ', 'MALDIVES            ', ' ', '  '),
(53, 'GUA', '0000-00-00', '        ', 'GUATEMALA           ', ' ', '  '),
(54, 'KZK', '0000-00-00', '        ', 'KAZAKHSTAN          ', ' ', '  '),
(55, 'LES', '0000-00-00', '        ', 'LESOTHO             ', ' ', '  '),
(56, 'PPN', '0000-00-00', '        ', 'PAPUA NEW           ', ' ', '  '),
(57, 'GUI', '0000-00-00', '        ', 'GUINEA              ', ' ', '  '),
(58, 'UKR', '0000-00-00', '        ', 'UKRAINE             ', ' ', '  '),
(59, 'ALG', '0000-00-00', '        ', 'ALGERIA             ', ' ', '  '),
(60, 'ARG', '0000-00-00', '        ', 'ARGENTINA           ', ' ', '  '),
(61, 'AU1', '0000-00-00', '        ', 'AUSTRIA             ', ' ', '  '),
(62, 'BA1', '0000-00-00', '        ', 'BAHAMAS             ', ' ', '  '),
(63, 'BAR', '0000-00-00', '        ', 'BARBADOS            ', ' ', '  '),
(64, 'BE1', '0000-00-00', '        ', 'BELIZE              ', ' ', '  '),
(65, 'BEN', '0000-00-00', '        ', 'BENIN               ', ' ', '  '),
(66, 'BOL', '0000-00-00', '        ', 'BOLIVIA             ', ' ', '  '),
(67, 'BOT', '0000-00-00', '        ', 'BOTSWANA            ', ' ', '  '),
(68, 'BUL', '0000-00-00', '        ', 'BULGARIA            ', ' ', '  '),
(69, 'BUF', '0000-00-00', '        ', 'BURKINA FASO        ', ' ', '  '),
(70, 'BU1', '0000-00-00', '        ', 'BURUNDI             ', ' ', '  '),
(71, 'CAM', '0000-00-00', '        ', 'CAMEROON            ', ' ', '  '),
(72, 'CHI', '0000-00-00', '        ', 'CHILE               ', ' ', '  '),
(73, 'COL', '0000-00-00', '        ', 'COLOMBIA            ', ' ', '  '),
(74, 'CON', '0000-00-00', '        ', 'CONGO               ', ' ', '  '),
(75, 'COS', '0000-00-00', '        ', 'COSTA RICA          ', ' ', '  '),
(76, 'CYP', '0000-00-00', '        ', 'CYPRUS              ', ' ', '  '),
(77, 'CZE', '0000-00-00', '        ', 'CZECH REPUBLIC      ', ' ', '  '),
(78, 'DEN', '0000-00-00', '        ', 'DENMARK             ', ' ', '  '),
(79, 'EST', '0000-00-00', '        ', 'ESTONIA             ', ' ', '  '),
(80, 'ETH', '0000-00-00', '        ', 'ETHOPIA             ', ' ', '  '),
(81, 'FIJ', '0000-00-00', '        ', 'FIJI                ', ' ', '  '),
(82, 'FIN', '0000-00-00', '        ', 'FINLAND             ', ' ', '  '),
(83, 'GAB', '0000-00-00', '        ', 'GABON               ', ' ', '  '),
(84, 'GHA', '0000-00-00', '        ', 'GHANA               ', ' ', '  '),
(85, 'GRE', '0000-00-00', '        ', 'GREECE              ', ' ', '  '),
(86, 'GRN', '0000-00-00', '        ', 'GRENADA             ', ' ', '  '),
(87, 'GUM', '0000-00-00', '        ', 'GUAM                ', ' ', '  '),
(88, 'GUY', '0000-00-00', '        ', 'GUYANA              ', ' ', '  '),
(89, 'NOA', '0000-00-00', '        ', 'NORTH AMERICA       ', ' ', '  '),
(90, 'LEB', '0000-00-00', '        ', 'LEBANON             ', ' ', '  '),
(91, 'BLR', '0000-00-00', '        ', 'BELARUS             ', ' ', '  '),
(92, 'CMB', '0000-00-00', '        ', 'CAMBODIA            ', ' ', '  '),
(93, 'IRE', '0000-00-00', '        ', 'IRELAND             ', ' ', '  '),
(94, 'JAM', '0000-00-00', '        ', 'JAMAICA             ', ' ', '  '),
(95, 'PHL', '0000-00-00', '        ', 'PHILIPPINES         ', ' ', '  '),
(96, 'QTR', '0000-00-00', '        ', 'QATAR               ', ' ', '  '),
(97, 'SWL', '0000-00-00', '        ', 'SWITZERLAND         ', ' ', '  '),
(98, 'ASP', '0000-00-00', '        ', 'ASIA PACIFIC        ', ' ', '  '),
(99, 'SAU', '2006-07-17', '17:02:17', 'SAUDI ARABIA        ', 'E', '01'),
(100, 'NZ ', '2007-04-05', '15:47:40', 'NEW ZEALAND         ', 'E', '01'),
(101, 'YEM', '2008-03-28', '11:48:56', 'YEMEN               ', 'E', '01'),
(102, 'TUR', '2010-07-16', '11:57:18', 'TURKEY              ', 'A', '01'),
(103, 'SKO', '2011-03-17', '14:55:28', 'SOUTH KOREA         ', 'A', '01'),
(104, 'LIE', '2014-06-10', '18:03:54', 'LIECHTENSTEIN       ', 'A', '01'),
(105, 'SLO', '2017-01-27', '14:12:21', 'Slovenia            ', 'A', '01'),
(106, 'TAN', '2017-02-07', '17:14:52', 'TANZANIA            ', 'A', '01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
