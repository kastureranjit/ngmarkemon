import { Injectable } from "@angular/core";
import { Http, Response, Headers } from "@angular/http";
import "rxjs/add/operator/map";

@Injectable()
export class AuthenticationService {
   // _url = 'http://localhost:3000/users/authenticate/';
    loginResponse : any;
    authToken : any;
    user : any;

    constructor(private http: Http) {
    }

    login(email: string, password: string) {
        let user = {
            email : email,
            password : password
        }
        let headers = new Headers();
        /*headers.append('Content-Type', 'application/json; charset=utf-8');
        headers.append('X-Requested-With','XMLHttpRequest'); 
        return this.http.post('http://localhost:3000/users/authenticate', user,{headers:headers})
        .subscribe(res=>{
            this.loginResponse = res.json(),
            console.log("In http call data is: "+res.json().success);
            console.log("In http call2 data is: "+this.loginResponse.success);
        });*/
        headers.append('Content-Type', 'application/json; charset=utf-8');
        headers.append('X-Requested-With','XMLHttpRequest'); 
       // headers.append('Authorization','Bearer '+localStorage.getItem('id_token'));
        return this.http.post('http://localhost:3000/users/authenticate',user,{headers:headers}).map((response: Response) => response.json());
       // console.log("data 3:"+this.loginResponse.success);
        //return this.loginResponse;
    }

    storeUserData(token,userData)
    {
    localStorage.setItem('id_token',token);
    localStorage.setItem('user',JSON.stringify(userData));
    this.authToken = token;
    this.user = userData;
    }

    logout()
    {
        this.authToken = null;
        this.user = null;
        localStorage.clear();
    }
}