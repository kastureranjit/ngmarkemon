var express = require('express'); 
var app = express(); 

var bodyParser = require('body-parser'); 
//const morgan = require('morgan');
var expressValidator = require('express-validator');

console.log("Entered login.js");

app.use("/test",(req,resp,next)=>{
  resp.status(200).json({
		message: 'It works here!'
  });
  
  
});



//Require the Router we defined in movies.js 
var loginController = require('./version/api/login'); //Use the Router on the sub route 


console.log("Entered app.js");



//for loogiging 
//app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json()); 
app.use(expressValidator());


//middleware for cors -ensures to prevent cors error
app.use((req,res,next) =>{
	res.header('Access-Control-Allow-Origin','*'); //cors  access to any origin
	res.header('Access-Control-Allow-Headers','Origin, X-Requested-With,Content-Type,Accept,Authorization');

		if (req.method === 'OPTIONS') 
		{
			req.header('Access-Control-Allow-Methods','PUT','POST','PATCH','DELETE','GET');
			return res.status(200).json({});
		}

		next();

});



app.use('/login',loginController);

//app.post("/login",loginController);


//any other req
app.use((req, res, next) => {

	const error  = new Error('Not found');
	error.status = 404;
	next(error);

  
});

app.use((error,req,res,next) =>{

	res.status(error.status || 500);
	res.json({
		error :{
			message: error.message
		}
	});
});


 
//To make this script app as module
module.exports = app;
//app.listen(3000);


