var mysql = require('mysql');

module.exports = {
    url : "mongodb://localhost:27017/mean"
}

var connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : 'census_db_v1'
  });
  connection.connect(function(err){
  if(!err) {
      console.log("Mysql is connected");
  } else {
      console.log("Error while connecting with database");
  }
  });
  module.exports = connection;