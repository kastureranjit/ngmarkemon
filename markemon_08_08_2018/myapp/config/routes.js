var users = require('../app/controllers/users');
var map = require('../app/controllers/map');
const checkAuth = require('../app/middleware/check-auth');

module.exports = function (app) {

    // Web API
    app.post('/users/register',users.signup);
    app.get('/users/profile',checkAuth,users.profile)
    app.post('/users/authenticate',users.login)

    // App API

    /* Map API starts here */
    app.get('/map/index',map.index)
    /* Map API ends here */
}
