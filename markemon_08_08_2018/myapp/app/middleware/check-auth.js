var jwt = require('jsonwebtoken');
const config = require('../../config/database');
const constant = require('./../../config/constants');

module.exports = (req,res,next)=>{
 try {
     const token = req.headers.authorization.split(" ")[1];
     const decoded = jwt.verify(token,constant.secret);
    req.userData = decoded;
    next();
 }
 catch(error)
 {
    res.json({
        success : false,
        msg : "Unauthorized"
    });
 }
    
}