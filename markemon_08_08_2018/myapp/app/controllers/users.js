const express = require('express');
const router = express.Router();
var jwt = require('jsonwebtoken');

const UserModel = require('../models/users');

var connection = require('./../../config/database');

const constant = require('./../../config/constants');

exports.signup = function(req, res) 
{
	let newUser = {
        vendor_name : req.body.name,
        vendor_user_name : req.body.username,
        vendor_user_password : UserModel.generateHash(req.body.password),
        vendor_user_email : req.body.email,
        status : 'Pending',
        created_at : new Date()
       }
        connection.query('INSERT INTO vendor_user_master SET ?',newUser, function (error, results, fields) {
            if (error) 
            {
                response = {
                    status:false,
                    message:error
                }
            }
            else
            {
                response = {
                    status:true,
                    message:"Registration Successfull"
                }
            }       
            return res.json(response);
        });
}



exports.profile = function(req,res)
{  
       /*console.log(req.userData.id);
       const userId = req.userData.id;
       UserModel.findById(userId, function (err, user) {
           console.log(user);
           if(err)
           {
              res.json({
                  success : false,
                  msg : err
              });  
           }
           if(!user)
           {
              res.json({
                  success : false,
                  msg : "User not found"
              });
           }
           else{
              res.json({
                  success : true,
                  msg : "You are authorized"
              });
           }
          });*/
}


exports.login = function(req, res) {
	/*let email = req.body.email;
    let password = req.body.password;
    UserModel.getUserByUsername(email, (err,user)=>{
    if(err)
    {
        res.json({
            success : false,
            msg : err
        });
    }
    if(!user)
    {
        res.json({
            success : false,
            msg : "User not found"
        });
    }
    else{
        UserModel.comparePassword(password, user.password,(err, isMatch)=>{
        
            if(err)
            {
                res.json({
                    success : false,
                    msg : err
                });
            }
            if(isMatch)
            {
            
                var token = jwt.sign({ id: user._id },constant.secret,{
                    expiresIn : 604800 // 1 weeks
                });

                res.json({
                    success : true,
                    token : token,
                    user : {
                        id : user._id,
                        name : user.name,
                        username : user.username,
                        email : user.email
                    }
                });
            }
            else{
                res.json({
                    success : false,
                    msg : "Wrong details"
                });
            }
        });
    }
   });*/
}   