import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ThemeComponent } from './theme/theme.component';
import { LayoutModule } from './theme/layouts/layout.module';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ScriptLoaderService } from "./_services/script-loader.service";
import { DataService } from "./_services/data.service";
import { ThemeRoutingModule } from "./theme/theme-routing.module";
import { AuthModule } from "./auth/auth.module";
import { HttpClientModule } from '@angular/common/http';
/* import { MapFiltersComponent } from './theme/pages/default/map-filters/map-filters.component'; */
//import { MapComponent } from './theme/pages/default/map/map.component';

@NgModule({
    declarations: [
        ThemeComponent,
        AppComponent,
        // MapFiltersComponent,
        //MapComponent,
    ],
    imports: [
        LayoutModule,
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        ThemeRoutingModule,
        AuthModule,
        HttpClientModule
    ],
    providers: [ScriptLoaderService, DataService],
    bootstrap: [AppComponent]
})
export class AppModule { }