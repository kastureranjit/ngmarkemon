import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';
import { Http } from '@angular/http';
import { DataService, Person } from '../../../../_services/data.service';
import { map } from 'rxjs/operators';
import * as $ from 'jquery';
import { } from '@types/googlemaps';

@Component({
    selector: 'app-map-filters',
    templateUrl: './map-filters.component.html',
    styleUrls: ['./map-filters.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class MapFiltersComponent implements OnInit, AfterViewInit {
    public show: boolean = false;
    public buttonName: any = 'Show map filters';
    public jsondata: any = [];
    public statejsondata: any = [];
    public districtServerData: any = [];
    public cityServerData: any = [];
    public censusServerData: any = [];
    public districtJsonData: any = [];
    public selectedDay: any;

    public dropdownList: any[] = [];


    public selectedItems = [];
    public statedropdownSettings: any = {};

    public selectedDistrictItems: any = [];
    public dropdownDistrictSettings: any = {};
    public dropdownCitySettings: any = {};
    public cityjsondata: any = [];

    constructor(private _script: ScriptLoaderService, private http: Http, private dataService: DataService) {

    }


    ngOnInit() {
        this.getstates();



        /* Code for populating state starts here */

        //console.log("Initial data is: "+this.jsondata);
        /* state dropdown settings starts here */
        this.statedropdownSettings = {
            singleSelection: false,
            idField: 'state_code',
            textField: 'state_name',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 3,
            allowSearchFilter: true,
            closeDropDownOnSelection: true
        };
        /* state dropdown settings ends here */

        /* district dropdown settings starts here */
        this.dropdownDistrictSettings = {
            singleSelection: false,
            idField: 'district_code',
            textField: 'district_name',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 3,
            allowSearchFilter: true,
            closeDropDownOnSelection: true
        };
        /* district dropdown settings ends here */

        /* city dropdown settings starts here */
        this.dropdownCitySettings = {
            singleSelection: false,
            idField: 'city_code',
            textField: 'city_name',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 3,
            allowSearchFilter: true,
            closeDropDownOnSelection: true
        };
        /* city dropdown settings ends here */

        /* Code for populating state ends here */

        /* jQuery code starts here */
        $(document).ready(function(){
           
        /* For census tab code starts here */ 
           $(document).on("click", "#m_widget4_tab1_content_li" , function() {
               console.log("In dynamic binding census info");
               $('#tab_heading_caption').text("Census Data"); 

            });
        /* For census tab code ends here */   

        /* For Media tab code starts here */
            $(document).on("click", "#m_widget4_tab2_content_li" , function() {
                let city_code = $(this).attr("data-citycode");
               
                $('#tab_heading_caption').text("Media Data Count");

                $.ajax({
                    url: 'http://localhost/census_db/get-press-count',
                    type: 'POST',
                    data: {city_code: city_code},
                    success: function(response)
                    {
                        let dt = JSON.parse(response);
                        $("#print_count").text(dt.press_count);
                        $("#ooh_count").text(dt.ooh_count);
                        $("#cinema_count").text(dt.cinema_count);
                       
                        /* Setting print option counts starts here */
                       
                        $('#print_listing_count').text('('+dt.press_option_count.publication_count+')');
                        $('#print_category_count').text('('+dt.press_option_count.category_count+')');
                        $('#print_format_count').text('('+dt.press_option_count.format_count+')');
                        $('#print_language_count').text('('+dt.press_option_count.language_count+')');
                        $('#print_frequency_count').text('('+dt.press_option_count.frequency_count+')');
                       
                        /* Setting print option counts ends here */
                       
                        
                        /* setting radio options  starts here */
                        
                        /* setting radio options ends here */


                        /* setting ooh options  starts here */
                        $('#ooh_location_count').text('('+dt.ooh_option_count.site_count+')');
                        $('#ooh_company_count').text('('+dt.ooh_option_count.company_count+')');
                        /* setting ooh options ends here */


                        /* setting cinema options  starts here */
                        $('#cinema_category_count').text('('+dt.cinema_option_count.category_count+')');
                        $('#cinema_language_count').text('(0)');
                        $('#cinema_location_count').text('('+dt.cinema_option_count.location_count+')'); 
                        /* setting cinema options ends here */


                        console.log("I am in press count http call and cinema count is: "+dt.cinema_count);
                        console.log("press option count is: "+dt.press_option_count.publication_count);
                    }
                });	

            });
        /* For Media tab code ends here */

        /* For BTL tab code starts here  */
            $(document).on("click", "#m_widget4_tab3_contentm_widget4_tab3_content_li" , function() {
                console.log("In dynamic binding BTL info");
                $('#tab_heading_caption').text("BTL Data");
            });
        /* For BTL tab code ends here */
        });
      
        /* jQuery code ends here */
    }

    /* functions  for state dropdown starts here  */
    onStateSelect(item: any) {
        this.statejsondata.push(item.state_code);
        this.getdistricts();

    }

    onStateDeSelect(item: any) {
        this.statejsondata.splice(this.statejsondata.indexOf(item.state_code), 1);
        if (this.statejsondata.length == 0) {
            this.statejsondata = [];
            this.districtServerData = [];
        }
        else {
            this.getdistricts();
        }

    }

    onStateSelectAll(items: any) {
        this.statejsondata = [];
        for (let item in items) {
            this.statejsondata.push(items[item]['state_code']);
        }
        this.getdistricts();
    }

    onStateDeSelectAll(items: any) {
        this.statejsondata = [];
        this.districtServerData = [];
    }

    /* functions  for state dropdown ends here */

    /* functions for district dropdown starts here */

    onDistrictSelect(item: any) {
        this.selectedDistrictItems.push(item.district_code);
        this.getcities();
    }

    onDistrictDeSelect(item: any) {
        this.selectedDistrictItems.splice(this.selectedDistrictItems.indexOf(item.district_code), 1);
        if (this.selectedDistrictItems.length == 0) {
            this.selectedDistrictItems = [];
            this.cityServerData = [];
        }
        else {
            this.getcities();
        }
    }

    onDistrictSelectAll(items: any) {
        this.selectedDistrictItems = [];
        for (let item in items) {
            this.selectedDistrictItems.push(items[item]['district_code']);
        }
        this.getcities();

    }

    onDistrictDeSelectAll(items: any) {
        this.selectedDistrictItems = [];
        this.cityServerData = [];
    }

    /* functions for district dropdown ends here */


    /* Functions for city dropdown starts here */

    onCitySelect(item: any) {
        console.log("Before push: "+this.cityjsondata);
        this.cityjsondata.push(item.city_code);
        console.log("After push: "+this.cityjsondata);
        console.log("Before http request: "+this.censusServerData);
        this.getcensusdata();
        console.log("After http request: "+this.censusServerData);
        //this.loadmapwithdata();

    }

    onCityDeSelect(item: any) {
        this.cityjsondata.splice(this.cityjsondata.indexOf(item.city_code), 1);
        if (this.cityjsondata.length == 0) {
            this.cityjsondata = [];
            this.censusServerData = [];
        }
        else {
            this.getcensusdata();
            //this.getcensusdata();
        }
    }

    onCitySelectAll(items: any) {
        this.cityjsondata = [];
        for (let item in items) {
            this.cityjsondata.push(items[item]['city_code']);
        }
        this.getcensusdata();
        //this.getcensusdata();
    }

    onCityDeSelectAll() {
        this.cityjsondata = [];
    }

    /* Functions for city dropdown ends here */



    getstates() {
        this.http.get('http://localhost/census_db/get-states')
            .subscribe(response => {
                this.jsondata = response.json();
            });
    }

    getdistricts() {
        this.http.post('http://localhost/census_db/get-districts-for-ng', { state_code: this.statejsondata })
            .subscribe(res => { this.districtServerData = res.json() });
    }

    getcities() {
        this.http.post('http://localhost/census_db/get-cities', { district_code: this.selectedDistrictItems })
            .subscribe(res => { this.cityServerData = res.json() });

    }

    async getcensusdata() {
        this.http.post('http://localhost/census_db/get-city-data', { city_code: this.cityjsondata })
            .subscribe(res => { 
                                this.censusServerData = res.json();
                                if(this.censusServerData.code != ''){
                                    this.loadmapwithdata();
                                } 
                            });
        console.log("Server data from http mehod call: "+this.censusServerData);    
    }

    


    loadmapwithdata() {
        if(this.statejsondata.length==0 || this.selectedDistrictItems.length==0 ||  this.cityjsondata.length==0 ){
            console.log("In if");
        }
        else{
            console.log("In else");
            /* Map starts here */
                    let centerlat;
                    let centerlng;
                    console.log("First index is: "+this.cityjsondata[0]);
                    let mapcenterlatlng = JSON.stringify(this.censusServerData);
                    for(let mapcenterlatlng of this.censusServerData){
                        if(mapcenterlatlng.city_code==this.cityjsondata[0]){
                            /* centercoordinate = new google.maps.LatLng(mapcenterlatlng.latitude,mapcenterlatlng.longitude); */
                            centerlat = mapcenterlatlng.latitude;
                            centerlng = mapcenterlatlng.longitude;
                        }
                    }
                    console.log("Latitude: "+centerlat+" longitude: "+centerlng);
                    var map = new google.maps.Map(document.getElementById("m_gmap_1"), {
                        //center: new google.maps.LatLng(20.7, 78.2),
                        center:new google.maps.LatLng(centerlat, centerlng),
                        zoom: 8,
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        fullscreenControl:true
                    });

                    //let json = this.cityServerData;

                    let parsedata = JSON.stringify(this.censusServerData);
                    console.log("census server data: " + JSON.stringify(this.censusServerData));
                    var infoWindow = new google.maps.InfoWindow({maxWidth: 600});
                    for (let parsedata of this.censusServerData) {
                        console.log("lat: " + parsedata.state_name);

                        /* Rendering data on google map starts here */

                        var latLng = new google.maps.LatLng(parsedata.latitude, parsedata.longitude);
                        // Rendering Markers starts here
                        var marker = new google.maps.Marker({
                            position: latLng,
                            map: map,
                            //zoom: '6',
                            animation: google.maps.Animation.DROP,
                            title: 'City Name: ' + parsedata.city_name,
                            // title: 'Demo Information'
                            icon: 'assets/app/media/img/mapicon.png'
                        });

                        (function(marker, parsedata) {
                            console.log("Data inside info window" + parsedata);
                            // Attaching a click event to the current marker
                            google.maps.event.addListener(marker, "mouseover", function(e) {
                                infoWindow.setContent(
                                /* row starts here */
                                '<div class="row">'+
                                    /* xl-4 starts here */
                                    '<div class="col-xl-12">'+
                                        /* full height tab starts here */
                                                '<div class="m-portlet m-portlet--full-height">'+
                                                    /* Tab heading starts here */
                                                    '<div class="m-portlet__head">'+
                                                        '<div class="m-portlet__head-caption">'+
                                                            '<div class="m-portlet__head-title">'+
                                                                '<h3 class="m-portlet__head-text"><span id="tab_heading_caption">Census Data </span></h3></div>'+
                                                        '</div>'+
                                                        '<div class="m-portlet__head-tools">'+
                                                            '<ul class="nav nav-pills nav-pills--brand m-nav-pills--align-right m-nav-pills--btn-pill m-nav-pills--btn-sm" role="tablist">'+
                                                                '<li class="nav-item m-tabs__item"> <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_widget4_tab1_content" id="m_widget4_tab1_content_li" role="tab">Census</a></li>'+
                                                                '<li class="nav-item m-tabs__item" id="m_widget4_tab2_content_li" data-citycode="'+parsedata.mw_city_code+'"> <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_widget4_tab2_content"role="tab">Media</a></li>'+
                                                                '<li class="nav-item m-tabs__item" id="m_widget4_tab3_contentm_widget4_tab3_content_li" data-citycode="'+parsedata.mw_city_code+'"> <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_widget4_tab3_content" role="tab" id="m_widget4_tab3_contentm_widget4_tab3_content_li">BTL</a> </li>'+
                                                            '</ul>'+
                                                        '</div>'+
                                                    '</div>'+
                                                    /* Tab heading ends here */
                                
                                                    /* whole tab body starts here */
                                                        '<div class="m-portlet__body">'+
                                                            /* tab content starts here */
                                                                '<div class="tab-content">'+
                                                                    /* tab1 starts here */
                                                                        '<div class="tab-pane active" id="m_widget4_tab1_content">'+
                                                                            '<div class="m-scrollable" data-scrollable="true" data-height="250" style="height: 250px; overflow: hidden;">'+
                                                                                '<div class="m-list-timeline m-list-timeline--skin-light">'+
                                                                                    '<div class="m-list-timeline__items">'+
                                                                                       /* Item1 starts here */ 
                                                                                            '<div class="m-list-timeline__item"> <span class="m-list-timeline__badge m-list-timeline__badge--info"></span> <span class="m-list-timeline__text">City: <span class="m-badge m-badge--success m-badge--wide">'+parsedata.city_name+'</span></span>'+
                                                                                            '</div>'+
                                                                                        /* Item1 ends here */


                                                                                        /* Item2 starts here */
                                                                                            '<div class="m-list-timeline__item"> <span class="m-list-timeline__badge m-list-timeline__badge--info"></span> <span class="m-list-timeline__text">District:  <span class="m-badge m-badge--success m-badge--wide">'+parsedata.district_name+'('+parsedata.state_name+')</span></span>'+
                                                                                            '</div>'+
                                                                                        /* Item2 ends here */


                                                                                        /* Item3 starts here */
                                                                                            '<div class="m-list-timeline__item"> <span class="m-list-timeline__badge m-list-timeline__badge--info"></span> <span class="m-list-timeline__text">Total :  <span class="m-badge m-badge--success m-badge--wide">'+parsedata.city_population+'</span></span>'+
                                                                                            '</div>'+   
                                                                                        /* Item3 ends here */


                                                                                        /* Item4 starts here */
                                                                                            '<div class="m-list-timeline__item"> <span class="m-list-timeline__badge m-list-timeline__badge--info"></span> <span class="m-list-timeline__text">Literate :  <span class="m-badge m-badge--success m-badge--wide">'+parsedata.city_literates_population+'('+Math.round((100*parseInt(parsedata.city_literates_population))/parseInt(parsedata.city_population))+'%)</span></span>'+
                                                                                            '</div>'+ 
                                                                                        /* Item4 ends here */


                                                                                        /* Item5 starts here */
                                                                                            '<div class="m-list-timeline__item"> <span class="m-list-timeline__badge m-list-timeline__badge--info"></span> <span class="m-list-timeline__text"><i class="fas fa-male" style="font-size: 20px;"></i> :  <span class="m-badge m-badge--success m-badge--wide">'+parsedata.city_male_population+'('+Math.round((100*parseInt(parsedata.city_male_population))/parseInt(parsedata.city_population))+'%)</span></span>'+
                                                                                            '</div>'+
                                                                                        /* Item5 ends here */


                                                                                        /* Item6 starts here */
                                                                                            '<div class="m-list-timeline__item"> <span class="m-list-timeline__badge m-list-timeline__badge--info"></span> <span class="m-list-timeline__text"><i class="fas fa-female" style="font-size: 20px;"></i> :  <span class="m-badge m-badge--success m-badge--wide">'+parsedata.city_female_population+'('+Math.round((100*parseInt(parsedata.city_female_population))/parseInt(parsedata.city_population))+'%)</span></span>'+
                                                                                            '</div>'+
                                                                                        /* Item6 ends here */
                                                                                    '</div>'+
                                                                                '</div>'+
                                                                            '</div>'+
                                                                        '</div>'+
                                                                    /* tab1 ends here */
                                
                                                                    /* tab2 starts here */
                                                                        '<div class="tab-pane" id="m_widget4_tab2_content">'+ 
                                                                            '<div class="m-scrollable" data-scrollable="true" data-height="260" style="height: 260px; overflow: hidden;">'+
                                                                            '<div class="m-list-timeline m-list-timeline--skin-light">'+
                                                                                '<div class="m-list-timeline__items">'+
                                                                                /* Item1 starts here */ 
                                                                                        '<div class="m-list-timeline__item"> <span class="m-list-timeline__badge m-list-timeline__badge--info"></span> <span class="m-list-timeline__text">Print <span class="m-badge m-badge--success m-badge--wide" id="print_count"></span></span>'+
                                                                                        /* Right side ellipses starts here */
                                                                                            '<div class="m-widget2__actions">'+
                                                                                            '<div class="m-widget2__actions-nav">'+
                                                                                                '<div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover"> <a href="#" class="m-dropdown__toggle"><i class="la la-ellipsis-v"></i></a>'+
                                                                                                    '<div class="m-dropdown__wrapper"> <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>'+
                                                                                                        '<div class="m-dropdown__inner">'+
                                                                                                            '<div class="m-dropdown__body">'+
                                                                                                                '<div class="m-dropdown__content">'+
                                                                                                                    '<ul class="m-nav">'+
                                                                                                                        '<li class="m-nav__item"> <a href="" class="m-nav__link"><i class="m-nav__link-icon flaticon-share"></i><span class="m-nav__link-text">Listing <span id="print_listing_count"> </span></span></a>'+ 
                                                                                                                        '</li>'+
                                                                                                                        '<li class="m-nav__item"> <a href="" class="m-nav__link"><i class="m-nav__link-icon flaticon-share"></i><span class="m-nav__link-text">Category <span id="print_category_count"> </span></span></a>'+ 
                                                                                                                        '</li>'+
                                                                                                                        '<li class="m-nav__item"> <a href="" class="m-nav__link"><i class="m-nav__link-icon flaticon-share"></i><span class="m-nav__link-text">Format <span id="print_format_count"></span> </span></a>'+ 
                                                                                                                        '</li>'+
                                                                                                                        '<li class="m-nav__item"> <a href="" class="m-nav__link"><i class="m-nav__link-icon flaticon-share"></i><span class="m-nav__link-text">Language <span id="print_language_count"></span> </span></a>'+ 
                                                                                                                        '</li>'+
                                                                                                                        '<li class="m-nav__item"> <a href="" class="m-nav__link"><i class="m-nav__link-icon flaticon-share"></i><span class="m-nav__link-text">Frequency <span id="print_frequency_count"></span> </span></a>'+ 
                                                                                                                        '</li>'+
                                                                                                                    '</ul>'+
                                                                                                                '</div>'+
                                                                                                            '</div>'+
                                                                                                        '</div>'+
                                                                                                    '</div>'+
                                                                                                '</div>'+
                                                                                            '</div>'+
                                                                                            '</div>'+
                                                                                        /* Right side ellipses ends here */
                                                                                        '</div>'+
                                                                                    /* Item1 ends here */


                                                                                    /* Item2 starts here */
                                                                                        '<div class="m-list-timeline__item"> <span class="m-list-timeline__badge m-list-timeline__badge--info"></span> <span class="m-list-timeline__text">Radio  <span class="m-badge m-badge--success m-badge--wide" id="radio_count"></span></span>'+
                                                                                        /* Right side ellipses starts here */
                                                                                            '<div class="m-widget2__actions">'+
                                                                                            '<div class="m-widget2__actions-nav">'+
                                                                                                '<div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover"> <a href="#" class="m-dropdown__toggle"><i class="la la-ellipsis-v"></i></a>'+
                                                                                                    '<div class="m-dropdown__wrapper"> <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>'+
                                                                                                        '<div class="m-dropdown__inner">'+
                                                                                                            '<div class="m-dropdown__body">'+
                                                                                                                '<div class="m-dropdown__content">'+
                                                                                                                    '<ul class="m-nav">'+
                                                                                                                        '<li class="m-nav__item"> <a href="" class="m-nav__link"><i class="m-nav__link-icon flaticon-share"></i><span class="m-nav__link-text">Listing <span id="radio_listing_count"></span> </span></a>'+ 
                                                                                                                        '</li>'+
                                                                                                                        '<li class="m-nav__item"> <a href="" class="m-nav__link"><i class="m-nav__link-icon flaticon-share"></i><span class="m-nav__link-text">Language <span id="radio_language_count"></span> </span></a>'+ 
                                                                                                                        '</li>'+
                                                                                                                        '<li class="m-nav__item"> <a href="" class="m-nav__link"><i class="m-nav__link-icon flaticon-share"></i><span class="m-nav__link-text">Category <span id="radio_category_count"></span> </span></a>'+ 
                                                                                                                        '</li>'+
                                                                                                                    '</ul>'+
                                                                                                                '</div>'+
                                                                                                            '</div>'+
                                                                                                        '</div>'+
                                                                                                    '</div>'+
                                                                                                '</div>'+
                                                                                            '</div>'+
                                                                                            '</div>'+
                                                                                        /* Right side ellipses ends here */
                                                                                        '</div>'+
                                                                                    /* Item2 ends here */


                                                                                    /* Item3 starts here */
                                                                                        '<div class="m-list-timeline__item"> <span class="m-list-timeline__badge m-list-timeline__badge--info"></span> <span class="m-list-timeline__text">OOH  <span class="m-badge m-badge--success m-badge--wide" id="ooh_count"></span></span>'+
                                                                                        /* Right side ellipses starts here */
                                                                                            '<div class="m-widget2__actions">'+
                                                                                            '<div class="m-widget2__actions-nav">'+
                                                                                                '<div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover"> <a href="#" class="m-dropdown__toggle"><i class="la la-ellipsis-v"></i></a>'+
                                                                                                    '<div class="m-dropdown__wrapper"> <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>'+
                                                                                                        '<div class="m-dropdown__inner">'+
                                                                                                            '<div class="m-dropdown__body">'+
                                                                                                                '<div class="m-dropdown__content">'+
                                                                                                                    '<ul class="m-nav">'+
                                                                                                                        '<li class="m-nav__item"> <a href="" class="m-nav__link"><i class="m-nav__link-icon flaticon-share"></i><span class="m-nav__link-text">Location <span id="ooh_location_count"></span> </span></a>'+ 
                                                                                                                        '</li>'+
                                                                                                                        '<li class="m-nav__item"> <a href="" class="m-nav__link"><i class="m-nav__link-icon flaticon-share"></i><span class="m-nav__link-text">Company <span id="ooh_company_count"></span> </span></a>'+ 
                                                                                                                        '</li>'+
                                                                                                                    '</ul>'+
                                                                                                                '</div>'+
                                                                                                            '</div>'+
                                                                                                        '</div>'+
                                                                                                    '</div>'+
                                                                                                '</div>'+
                                                                                            '</div>'+
                                                                                            '</div>'+
                                                                                        /* Right side ellipses ends here */
                                                                                        '</div>'+   
                                                                                    /* Item3 ends here */


                                                                                    /* Item4 starts here */
                                                                                        '<div class="m-list-timeline__item"> <span class="m-list-timeline__badge m-list-timeline__badge--info"></span> <span class="m-list-timeline__text">CINEMA  <span class="m-badge m-badge--success m-badge--wide" id="cinema_count"></span></span>'+
                                                                                        /* Right side ellipses starts here */
                                                                                            '<div class="m-widget2__actions">'+
                                                                                            '<div class="m-widget2__actions-nav">'+
                                                                                                '<div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover"> <a href="#" class="m-dropdown__toggle"><i class="la la-ellipsis-v"></i></a>'+
                                                                                                    '<div class="m-dropdown__wrapper"> <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>'+
                                                                                                        '<div class="m-dropdown__inner">'+
                                                                                                            '<div class="m-dropdown__body">'+
                                                                                                                '<div class="m-dropdown__content">'+
                                                                                                                    '<ul class="m-nav">'+
                                                                                                                        '<li class="m-nav__item"> <a href="" class="m-nav__link"><i class="m-nav__link-icon flaticon-share"></i><span class="m-nav__link-text">Category <span id="cinema_category_count"></span> </span></a>'+ 
                                                                                                                        '</li>'+
                                                                                                                        '<li class="m-nav__item"> <a href="" class="m-nav__link"><i class="m-nav__link-icon flaticon-share"></i><span class="m-nav__link-text">Language <span id="cinema_language_count"></span> </span></a>'+ 
                                                                                                                        '</li>'+
                                                                                                                        '<li class="m-nav__item"> <a href="" class="m-nav__link"><i class="m-nav__link-icon flaticon-share"></i><span class="m-nav__link-text">Location <span id="cinema_location_count"></span> </span></a>'+ 
                                                                                                                        '</li>'+
                                                                                                                    '</ul>'+
                                                                                                                '</div>'+
                                                                                                            '</div>'+
                                                                                                        '</div>'+
                                                                                                    '</div>'+
                                                                                                '</div>'+
                                                                                            '</div>'+
                                                                                            '</div>'+
                                                                                        /* Right side ellipses ends here */
                                                                                        '</div>'+ 
                                                                                    /* Item4 ends here */
                                                                                '</div>'+
                                                                            '</div>'+
                                                                        '</div>'+   
                                                                        '</div>'+
                                                                    /* tab2 ends here */
                                
                                                                    /* tab3 starts here */
                                                                        '<div class="tab-pane" id="m_widget4_tab3_content">BTL data will be displayed here'+ 
                                                                        '</div>'+
                                                                   /* tab3 ends here */ 
                                                                '</div>'+
                                                            /* tab content ends here */
                                                        '</div>'+
                                                    /* whole tab body ends here */
                                                '</div>'+
                                        /* full height tab ends here */
                                    '</div>'+
                                    /* xl-4 ends here */
                                '</div>'        
                                /* row ends here */
                            );
                                infoWindow.open(map, marker);
                            });


                        })(marker, parsedata);
                        /* Rendering data on google map ends here */
                    }
        }
        

        

    }

    /* toggle_census_info()
    {
        console.log("In toggle_census_info function");
    }
 */
    

    ngAfterViewInit() {
        this._script.loadScripts('app-map-filters',
            ['assets/demo/default/custom/crud/forms/widgets/select2.js']);

    }

    /* Code for toggling map filters starts here */
    toggle() {
        this.getstates();

        this._script.loadScripts('app-map-filters',
            ['assets/demo/default/custom/crud/forms/widgets/select2.js',
                'assets/demo/default/custom/components/maps/google-maps.js',
                'assets/app/js/dashboard.js']);

        this.show = !this.show;

        // CHANGE THE NAME OF THE BUTTON.
        if (this.show)
            this.buttonName = "Hide map filters";
        else
            this.buttonName = "Show map filters";
    }
    /* Code for toggling map filters ends here */

}
