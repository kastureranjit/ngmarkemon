import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../../helpers';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';
import { MapFiltersComponent } from '../map-filters/map-filters.component';

@Component({
    selector: 'app-map',
    templateUrl: './map.component.html',
    styleUrls: ['./map.component.scss'],
    encapsulation: ViewEncapsulation.None,
})


export class MapComponent implements OnInit, AfterViewInit {

    constructor(private _script: ScriptLoaderService) {

    }

    ngOnInit() {
    }

    ngAfterViewInit() {
        this._script.loadScripts('app-map',
            ['assets/vendors/custom/gmaps/gmaps.js',
                'assets/demo/default/custom/components/maps/google-maps.js',
                'assets/demo/default/custom/crud/forms/widgets/select2.js']);

    }

   
}
