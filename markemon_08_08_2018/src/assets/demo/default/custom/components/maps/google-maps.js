/* var GoogleMapsDemo = {
    init: function () {
        var t;
        new GMaps({
                div: "#m_gmap_1",
                lat: -12.043333,
                lng: -77.028333
            }), new GMaps({
                div: "#m_gmap_2",
                zoom: 16,
                lat: -12.043333,
                lng: -77.028333,
                click: function (t) {
                    alert("click")
                },
                dragend: function (t) {
                    alert("dragend")
                }
            }), (t = new GMaps({
                div: "#m_gmap_3",
                lat: -51.38739,
                lng: -6.187181
            })).addMarker({
                lat: -51.38739,
                lng: -6.187181,
                title: "Lima",
                details: {
                    database_id: 42,
                    author: "HPNeo"
                },
                click: function (t) {
                    console.log && console.log(t), alert("You clicked in this marker")
                }
            }), t.addMarker({
                lat: -12.042,
                lng: -77.028333,
                title: "Marker with InfoWindow",
                infoWindow: {
                    content: '<span style="color:#000">HTML Content!</span>'
                }
            }), t.setZoom(5),
            function () {
                var t = new GMaps({
                    div: "#m_gmap_4",
                    lat: -12.043333,
                    lng: -77.028333
                });
                GMaps.geolocate({
                    success: function (e) {
                        t.setCenter(e.coords.latitude, e.coords.longitude)
                    },
                    error: function (t) {
                        alert("Geolocation failed: " + t.message)
                    },
                    not_supported: function () {
                        alert("Your browser does not support geolocation")
                    },
                    always: function () {}
                })
            }(),
            function () {
                var t = new GMaps({
                    div: "#m_gmap_5",
                    lat: -12.043333,
                    lng: -77.028333,
                    click: function (t) {
                        console.log(t)
                    }
                });
                path = [
                    [-12.044012922866312, -77.02470665341184],
                    [-12.05449279282314, -77.03024273281858],
                    [-12.055122327623378, -77.03039293652341],
                    [-12.075917129727586, -77.02764635449216],
                    [-12.07635776902266, -77.02792530422971],
                    [-12.076819390363665, -77.02893381481931],
                    [-12.088527520066453, -77.0241058385925],
                    [-12.090814532191756, -77.02271108990476]
                ], t.drawPolyline({
                    path: path,
                    strokeColor: "#131540",
                    strokeOpacity: .6,
                    strokeWeight: 6
                })
            }(), new GMaps({
                div: "#m_gmap_6",
                lat: -12.043333,
                lng: -77.028333
            }).drawPolygon({
                paths: [
                    [-12.040397656836609, -77.03373871559225],
                    [-12.040248585302038, -77.03993927003302],
                    [-12.050047116528843, -77.02448169303511],
                    [-12.044804866577001, -77.02154422636042]
                ],
                strokeColor: "#BBD8E9",
                strokeOpacity: 1,
                strokeWeight: 3,
                fillColor: "#BBD8E9",
                fillOpacity: .6
            }),
            function () {
                var t = new GMaps({
                    div: "#m_gmap_7",
                    lat: -12.043333,
                    lng: -77.028333
                });
                $("#m_gmap_7_btn").click(function (e) {
                    e.preventDefault(), mUtil.scrollTo("m_gmap_7_btn", 400), t.travelRoute({
                        origin: [-12.044012922866312, -77.02470665341184],
                        destination: [-12.090814532191756, -77.02271108990476],
                        travelMode: "driving",
                        step: function (e) {
                            $("#m_gmap_7_routes").append("<li>" + e.instructions + "</li>"), $("#m_gmap_7_routes li:eq(" + e.step_number + ")").delay(800 * e.step_number).fadeIn(500, function () {
                                t.setCenter(e.end_location.lat(), e.end_location.lng()), t.drawPolyline({
                                    path: e.path,
                                    strokeColor: "#131540",
                                    strokeOpacity: .6,
                                    strokeWeight: 6
                                })
                            })
                        }
                    })
                })
            }(),
            function () {
                var t = new GMaps({
                        div: "#m_gmap_8",
                        lat: -12.043333,
                        lng: -77.028333
                    }),
                    e = function () {
                        var e = $.trim($("#m_gmap_8_address").val());
                        GMaps.geocode({
                            address: e,
                            callback: function (e, o) {
                                if ("OK" == o) {
                                    var n = e[0].geometry.location;
                                    t.setCenter(n.lat(), n.lng()), t.addMarker({
                                        lat: n.lat(),
                                        lng: n.lng()
                                    }), mUtil.scrollTo("m_gmap_8")
                                }
                            }
                        })
                    };
                $("#m_gmap_8_btn").click(function (t) {
                    t.preventDefault(), e()
                }), $("#m_gmap_8_address").keypress(function (t) {
                    "13" == (t.keyCode ? t.keyCode : t.which) && (t.preventDefault(), e())
                })
            }()
    }
};
jQuery(document).ready(function () {
    GoogleMapsDemo.init()
}); */


/* Custom map functions starts here */

$.ajax({
	url:'http://localhost/census_db/get-default-cities-for-map',
	type: 'POST',
	//dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
	data: {param1: 'value1'},
	success: function(response){
		console.log("I am in default cities data is: "+response);
		json = eval("(" + response + ")");
			    	/*####### Dynamic Google Map rendering starts here #######*/

	    		   // Creating a new map
                        var map = new google.maps.Map(document.getElementById("m_gmap_1"), {
                              center: new google.maps.LatLng(20.7, 78.2),
                              zoom: 6,
                              mapTypeId: google.maps.MapTypeId.ROADMAP
                              //mapTypeId: 'hybrid'
                            });
       
                        // Creating a global infoWindow object that will be reused by all markers
                          var infoWindow = new google.maps.InfoWindow();

                         for (var i = 0, length = json.length; i < length; i++) {
                            //var data = response.data[i],
                              latLng = new google.maps.LatLng(json[i].latitude, json[i].longitude);

                              // Rendering Markers starts here
                              		 var marker = new google.maps.Marker({
		                              position: latLng,
		                              map: map,
		                              animation: google.maps.Animation.DROP,
		                              title: 'City Name: '+json[i].city_name,
		                             /* title: 'Demo Information'*/
		                              //icon: icon
		                            });
                              // Rendering Markers ends here
                              	jsonn = json[i];
                               (function(marker, jsonn) {
                               		console.log("Data inside info window"+jsonn);
		                            // Attaching a click event to the current marker
		                            google.maps.event.addListener(marker, "mouseover", function(e) {
		                            	if(jsonn.earlier_name==='' || jsonn.earlier_name===null || jsonn.earlier_name===undefined)
		                            	{
		                            		earlier_name = "Data not available";
		                            	}
		                            	else
		                            	{
		                            		earlier_name = jsonn.earlier_name;
		                            	}

		                            	if(jsonn.other_names==='' || jsonn.other_names===null || jsonn.other_names===undefined)
		                            	{
		                            		other_names = "Data not available";
		                            	}
		                            	else
		                            	{
		                            		other_names = jsonn.other_names;
		                            	}

		                              infoWindow.setContent('<div class="scrollFix" style="line-height: 1.35;overflow: hidden;white-space: nowrap;">'+
		                                '<div class="row">'+
		                                          '<div class="col-md-12">'+
		                                          	'<div id="census_info">'+
			                                            '<p style="font-weight: bold;">City: '+jsonn.city_name+'</p>'+
			                                            '<p><span style="font-weight: bold;">District: </span><span>'+jsonn.district_name+'('+jsonn.state_name+')</span></p>'+
			                                            '<p><span style="font-weight: bold;">Total : </span><span>'+jsonn.city_population+'</span></p>'+
			                                            '<p><span style="font-weight: bold;">Literate : </span><span>'+jsonn.city_literates_population+'('+Math.round((100*parseInt(jsonn.city_literates_population))/parseInt(jsonn.city_population))+'%)</span></p>'+
			                                            '<p><span style="font-weight: bold;"><i class="fas fa-male" style="font-size: 20px;"></i> : </span><span>'+jsonn.city_male_population+'('+Math.round((100*parseInt(jsonn.city_male_population))/parseInt(jsonn.city_population))+'%)</span></p>'+
			                                            '<p><span style="font-weight: bold;"><i class="fas fa-female" style="font-size: 20px;"></i> : </span><span>'+jsonn.city_female_population+'('+Math.round((100*parseInt(jsonn.city_female_population))/parseInt(jsonn.city_population))+'%)</span></p>'+
			                                          	'<p style="font-weight: bold;color:#3C8CBB;cursor:pointer" onclick="showmodal(`'+jsonn.city_code+'`)">more>></p>'+
		                                          	'</div>'+
		                                          	'<div id="media_info" style="display:none;">'+
		                                          	'<p>I am in media info</p>'+
		                                          	'</div>'+
		                                          	'<div id="btl_info" style="display:none;">'+
		                                          	'<p>I am in btl info</p>'+
		                                          	'</div>'+
		                                          	'<p><span><button class="btn btn-primary map_modal" onclick="toggle_census_info()" id="toggle_census_info">Census</button></span>'+
			                                        '<span style="padding-left:10px;"><button class="btn btn-primary map_modal" onclick="toggle_media_info()" id="toggle_media_info">Media</button></span>'+
			                                        '<span style="padding-left:10px;"><button class="btn btn-primary map_modal" onclick="toggle_btl_info()" id="toggle_btl_info">BTL</button></span></p>'+
		                                          '</div>'+
		                                 '</div>'+

		                                '</div>');
		                              infoWindow.open(map, marker);
		                            });

		                            /* google.maps.event.addListener(marker, "mouseout", function(e) {
									    infoWindow.close();
									});*/

		                          })(marker, jsonn);
                         }  
	    	/*####### Dynamic Google Map rendering ends here #######*/
	}
})
.done(function() {
	console.log("success");
})
.fail(function() {
	console.log("error");
})
.always(function() {
	console.log("complete");
});

/* Custom map functions ends here */

function testcase()
{
    console.log("I am in google map.js testcase function");
}